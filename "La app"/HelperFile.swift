//
//  HelperFile.swift
//  "La app"
//
//  Created by Aldo Gutierrez Montoya on 1/10/19.
//  Copyright © 2019 Delberto Martinez. All rights reserved.
//

import Foundation
import UIKit
public class Alert {
    class func ShowAlert(title: String, message: String, titleForTheAction: String, in vc: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: titleForTheAction, style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
}
