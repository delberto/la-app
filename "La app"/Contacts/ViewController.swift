//
//  ViewController.swift
//  "La app"
//
//  Created by Aldo Gutierrez Montoya on 1/8/19.
//  Copyright © 2019 Delberto Martinez. All rights reserved.
//

import UIKit
import Foundation
import Contacts
import MessageUI
import CoreData
//Create a global variable for the object.
var searchResultSaved : [NSManagedObject] = []


//Create a struct for the subscribed Contacts
struct ContactSubscribed {
    var telephoneSubscribed : String = ""
    var nameSubscribed : String = ""
    var isSubscribed: Bool = false
    
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate, UISearchBarDelegate {
  
    //MARK:- VARIABLES & CONSTANTS
    //Declare all the varibales and constants needed.
    private let cell = "ContactsTableViewCell"
    var contactSubscribed = ContactSubscribed()
    var completeNames = [CompleteName]()
    
    //MARK:- OUTLETS
    //Here connect the outlets.
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var nameDetail: UILabel!
    
    @IBOutlet weak var telephoneDetail: UILabel!
    
    @IBOutlet weak var detailView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        
        //Call the method
        fetchContacts()
     
        //Connect delegate and dataSource
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       //Configure CoreData
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Search")
        
        //3
        do {
            searchResultSaved = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Error. \(error), \(error.userInfo)")
        }
        
        
    }
    
  
    
    
    //MARK:- METHODS.
    //Here declare all the methods.
 
    func saveTheSearch(name: String , telephone: String) {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        // 1
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: "Search",
                                       in: managedContext)!
        
      
        let search = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        // 3
        search.setValue(name, forKey: "name")
        search.setValue(telephone, forKey: "telephone")
        
        // 4
        do {
            try managedContext.save()
            searchResultSaved.append(search)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    //Ask permission to show the contacts.
    private func fetchContacts() {
        print("Attempting to fetch contacts today..")
        
        let store = CNContactStore()
        
        store.requestAccess(for: .contacts) { (granted, err) in
            if let err = err {
                print("Failed to request access:", err)
                return
            }
            
            if granted {
                print("Access granted")
                
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    
                    var favoritableContacts = [SubscribeContact]()
                    
                    try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointerIfYouWantToStopEnumerating) in
                        
                        print(contact.givenName)
                        print(contact.familyName)
                        print(contact.phoneNumbers.first?.value.stringValue ?? "")
                        
                        favoritableContacts.append(SubscribeContact(contact: contact, hasFavorited: false))
                        
                        //                        favoritableContacts.append(FavoritableContact(name: contact.givenName + " " + contact.familyName, hasFavorited: false))
                    })
                    
                    let names = CompleteName(isExpanded: true, isSubscribed: false, names: favoritableContacts)
                    self.completeNames = [names]
                    
                } catch let err {
                    print("Failed to enumerate contacts:", err)
                }
                
            } else {
                print("Access denied..")
            }
        }
    }
    
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return completeNames.count
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completeNames[section].names.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! ContactsTableViewCell
        let contacts = completeNames[indexPath.section].names[indexPath.row]
        
        cel.nameLabel.text = contacts.contact.givenName + " " + contacts.contact.familyName
        cel.telephoneLabel.text = contacts.contact.phoneNumbers.first?.value.stringValue
        
        // var contact = completeNames[indexPath.row].isSubscribed
        
        return cel
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 93.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //DESCOMENTAR PARA MOSTRAR LA VISTA
        //detailView.isHidden = false
        
       
        let addContact = completeNames[indexPath.section].names[indexPath.row]
      
       
        let name = addContact.contact.givenName + " " + addContact.contact.familyName
        
        let phoneNumber = addContact.contact.phoneNumbers.first?.value.stringValue
       
        contactSubscribed.telephoneSubscribed = phoneNumber ?? ""
        contactSubscribed.nameSubscribed = name
        
       

            let myAlert = UIAlertController(title: "", message: "¿Deseas agregar este usuario?", preferredStyle: .alert)
            
            let aceptar = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default) {
                UIAlertAction in
               
                self.saveTheSearch(name: self.contactSubscribed.nameSubscribed, telephone: self.contactSubscribed.telephoneSubscribed)
                
                if (MFMessageComposeViewController.canSendText()) {
                    let controller = MFMessageComposeViewController()
                    controller.body = "¡TE INVITO A PROBAR EL APP!"
                    controller.recipients = [self.contactSubscribed.telephoneSubscribed] 
                    controller.messageComposeDelegate = self
                    self.present(controller, animated: true, completion: nil)
                }
                
            }
            
            let cancelar = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                
              
            }
            
            //myAlert.addAction(cancelButton)
            myAlert.addAction(aceptar)
            myAlert.addAction(cancelar)
            
            self.present(myAlert, animated: true , completion: nil)

     
      

    }
    
    
    //MARK:-IBActions
    //Handle the data to present depending the index of the segmented control

    @IBAction func clickSuscritosButton(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "SubscribedViewController") as? SubscribedViewController
       self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    //Close the detailView
    @IBAction func clickCloseButton(_ sender: Any) {
        self.detailView.isHidden = true 
    }
    
    //Protocol of MFMessageComposeViewControllerDelegate
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        
        self.dismiss(animated: true, completion: nil)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
}

