//
//  SubscribedTableViewCell.swift
//  "La app"
//
//  Created by Aldo Gutierrez Montoya on 1/9/19.
//  Copyright © 2019 Delberto Martinez. All rights reserved.
//

import UIKit

class SubscribedTableViewCell: UITableViewCell {
   
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var telephoneNumber: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
