//
//  SubscribedViewController.swift
//  "La app"
//
//  Created by Aldo Gutierrez Montoya on 1/10/19.
//  Copyright © 2019 Delberto Martinez. All rights reserved.
//

import UIKit
import CoreData
class SubscribedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    private let cell = "SubscribedTableViewCell"
    

    override func viewDidLoad() {
        super.viewDidLoad()

       grayView.isHidden = true
        
        //set the title of the view
        self.navigationItem.title = "Contactos Suscritos."
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
     //Return Saved values in CoreData
        return  searchResultSaved.count
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! SubscribedTableViewCell
        let result = searchResultSaved[indexPath.row]
        
        cel.contactName.text = result.value(forKeyPath: "name") as? String
       cel.telephoneNumber.text = result.value(forKey: "telephone") as? String
        
        return cel
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! SubscribedTableViewCell
        grayView.isHidden = false
        let result = searchResultSaved[indexPath.row]

       cel.contactName.text = result.value(forKeyPath: "name") as? String
        nameLabel.text = cel.contactName.text
        cel.telephoneNumber.text = result.value(forKey: "telephone") as? String
        telephoneLabel.text = cel.telephoneNumber.text
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 119.0
    }

    //MARK:- OUTLETS
     @IBOutlet weak var nameLabel: UILabel!
     @IBOutlet weak var telephoneLabel: UILabel!
    @IBOutlet weak var grayView: UIView!
    
    //MARK:- IBACTIONS
    @IBAction func clickCloseButton(_ sender: Any) {
        
        self.grayView.isHidden = true
    }
    
   
    
}
