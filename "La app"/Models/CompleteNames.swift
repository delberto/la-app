//
//  CompleteNames.swift
//  "La app"
//
//  Created by Aldo Gutierrez Montoya on 1/9/19.
//  Copyright © 2019 Delberto Martinez. All rights reserved.
//

import Foundation
import Contacts

struct CompleteName{
    var isExpanded: Bool
    var isSubscribed: Bool = false 
    var names: [SubscribeContact]
}

struct SubscribeContact {
    let contact: CNContact
    var hasFavorited: Bool
    
}

struct Names {
   var name: String = ""
    var telephone: String = "" 
}
